
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class wazzap {

    static int biayaMobil = 20000;
    static int biayaMotor = 5000;
    static int biayaOjek = 10000;
//    static Date jamBerangkat;
    static int suhu;
    static String pilihanKendaraan;
    static Long waktuTiba;
    static int biaya;
    static int jamTraining = 8;

    public static void main(String[] args) throws ParseException {
        Scanner in = new Scanner(System.in);
        Calendar calendar = Calendar.getInstance();
        String pattern = "HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("idn", "IDN"));
        Format f = new SimpleDateFormat("HH:mm");

        Random rand = new Random();
        suhu = rand.nextInt(5) + 35;

        // masukan jam dan menit
        System.out.println("==Format jam HH:mm==");
        System.out.print("Jam berangkat = ");
        String jamBerangkat = in.nextLine();
        Date jamBerangkat1 = simpleDateFormat.parse(jamBerangkat);

        System.out.println("========");
        System.out.println("========");

        Date jamMobil = simpleDateFormat.parse("06:01");
        Date jamMotor = simpleDateFormat.parse("07:01");

        calendar.setTime(jamBerangkat1);


        if (jamBerangkat1.before(jamMobil)) {
            pilihanKendaraan = "Mobil";
            calendar.add(Calendar.MINUTE, 60);
            biaya = biayaMobil;
        } else if (jamBerangkat1.after(jamMobil) && jamBerangkat1.before(jamMotor)) {
            pilihanKendaraan = "Motor";
            calendar.add(Calendar.MINUTE, 45);

            biaya = biayaMotor;
        } else {
            pilihanKendaraan ="Ojek";
            calendar.add(Calendar.MINUTE, 40);
            biaya = biayaOjek;
        }

        String waktuTiba2 = f.format(calendar.getTime());

        calendar.add(Calendar.HOUR, 8);
        String waktuDirumah = f.format(calendar.getTime());

        System.out.println("Jam berangkat: " + jamBerangkat1 );
        System.out.println("Suhu: " + suhu + " derajat");
        System.out.println("Kendaraan yang digunakan: " + pilihanKendaraan);
        System.out.println("Biaya yang dikeluarkan: Rp " + biaya);
        System.out.println("Waktu sampai di rumah : "+waktuDirumah);
        System.out.println("Waktu tiba: " + waktuTiba2 );
        System.out.println("Total biaya yang dikeluarkan: " + biaya );
        in.close();
    }

}